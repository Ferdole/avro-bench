module example/avro-generation

go 1.20

require (
	github.com/linkedin/goavro v2.1.0+incompatible
	github.com/linkedin/goavro/v2 v2.12.0
)