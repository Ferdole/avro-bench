package main

import (
	"bytes"
	"fmt"
	"math"
	"math/rand"
	"os"

	"github.com/linkedin/goavro"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

var numberArray = [10]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}

func main() {
	filename := "../random_strings.avro"
	avroSchema := `{
	    "type": "record",
	    "name": "User",
	    "fields": [
	        {"name": "Name", "type": "string"},
	        {"name": "Age", "type": "int"},
			{"name": "Stuff", "type": {
				"type": "array",
				"items": "string"
			}, "default": []}
	    ]
	}`

	file, err := os.Create(filename)
	if err != nil {
		fmt.Println("Error creating file:", err)
		return
	}

	// // Create a new data structure conforming to the schema
	// users := []map[string]interface{}{
	// 	{
	// 		"Name":  "Alice",
	// 		"Age":   25,
	// 		"Stuff": []string{"some1", "some2", "some3"},
	// 	},
	// 	{
	// 		"Name":  "Dorel",
	// 		"Age":   22,
	// 		"Stuff": []string{"some1", "some2", "some3"},
	// 	},
	// }

	users := generateRandomRecords(1000000, 7, 18, 8)

	// fmt.Println(users)
	// Writing OCF data
	var ocfFileContents bytes.Buffer
	writer, err := goavro.NewOCFWriter(goavro.OCFConfig{
		W:      file,
		Schema: avroSchema,
	})
	if err != nil {
		fmt.Println(err)
	}
	err = writer.Append(users)
	fmt.Println("Err: ", err)
	fmt.Println("ocfFileContents", ocfFileContents.String())

	file.Close()

	// Open the file
	file_read, err := os.Open(filename)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}
	defer file_read.Close()

	// Reading OCF data
	// ocfReader, err := goavro.NewOCFReader(file_read)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// fmt.Println("Records in OCF File")

	// for ocfReader.Scan() {
	// 	record, err := ocfReader.Read()
	// 	if err != nil {
	// 		fmt.Println(err)
	// 	}
	// 	fmt.Println("record", record)
	// }

}

func generateRandomRecords(recordNumber, noStuff, stringMaxSize, intMaxSize int) []map[string]interface{} {
	records := make([]map[string]interface{}, recordNumber)
	for i := range records {
		records[i] = map[string]interface{}{
			"Name":  generateRandomString(stringMaxSize),
			"Age":   generateRandomNumber(intMaxSize),
			"Stuff": generateStringArray(noStuff, stringMaxSize),
		}
	}

	return records
}

func generateStringArray(noItems, maxStringsize int) []string {
	strings := make([]string, noItems)
	for i := range strings {
		strings[i] = generateRandomString(maxStringsize)
	}
	return strings
}

func generateRandomString(lenght int) string {
	stringBytes := make([]byte, lenght)
	for i := range stringBytes {
		stringBytes[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(stringBytes)
}

func generateRandomNumber(lenght int) int {
	number := 0
	for i := 0; i < lenght; i++ {
		number += numberArray[rand.Intn(len(numberArray))] * int(math.Pow(10, float64(i)))
	}

	return number
}
