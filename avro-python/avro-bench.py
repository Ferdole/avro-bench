import time
from io import BytesIO

from fastavro import block_reader, schemaless_reader, reader

SCHEMA = {
    "type": "record",
    "name": "User",
    "fields": [
        {"name": "Name", "type": "string"},
        {"name": "Age", "type": "int"},
        {"name": "Stuff", "type": {
            "type": "array",
            "items": "string"
        }, "default": []}
    ]
}
if __name__ == "__main__":

    with open("../random_strings.avro", 'rb') as fo:
        t0 = time.time()
        avro_reader = reader(fo)
        for block in avro_reader:
            if block:
                continue

        print((time.time() - t0) * 1000)
