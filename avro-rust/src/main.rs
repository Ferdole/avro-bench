use std::fs::File;
use std::io::{self, prelude::*, Read, BufReader};
use std::time::{SystemTime, UNIX_EPOCH};


use avro_rs::Reader;

fn main() {
    println!("Hello Avro");

    // TODO: let system do the duration
    let start = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_millis();

    // TODO: pass in as argument
    let file = File::open("../random_strings.avro").unwrap();

    let reader = Reader::new(
        BufReader::new(file)
    ).unwrap();

    for item in reader {
        match item {
            _ => continue,
        }
    }

    let end = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_millis();
    let fin = end - start;

    println!("fin: {:?} millis", fin);
}
