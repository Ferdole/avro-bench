module example/avro-go

go 1.20

require (
	github.com/linkedin/goavro v2.1.0+incompatible
	github.com/linkedin/goavro/v2 v2.12.0
)

require github.com/golang/snappy v0.0.1 // indirect
