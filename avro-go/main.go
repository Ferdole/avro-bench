package main

import (
	"fmt"
	"os"
	"time"

	"github.com/linkedin/goavro"
)

func main() {

	t0 := time.Now()
	// stringArray := string([]byte{115, 114})
	// fmt.Println(st[1])
	// fmt.Println(stringArray)
	// // Open the file
	b, err := os.Open("../random_strings.avro")
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}

	// fmt.Println(b)
	// Create a scanner to read the file line by line
	// scanner := bufio.NewScanner(file)
	// stringContents := strings.NewReader(string(b))
	// Reading OCF data
	ocfReader, err := goavro.NewOCFReader(b)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Records in OCF File")
	for ocfReader.Scan() {
		_, err := ocfReader.Read()
		if err != nil {
			fmt.Println(err)
		}
		// fmt.Println("record", record)
	}
	fmt.Println(" ", time.Since(t0))
}
