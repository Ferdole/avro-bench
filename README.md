## Benchamarking avro decoding in different languages

### For a 1 million entries file with pretty simple schema

- Rust: ~0.7s
- Go: ~1.3s
- Python: 2.4s